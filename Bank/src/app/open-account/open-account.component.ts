import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import {DataServiceService} from "../data-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-open-account',
  templateUrl: './open-account.component.html',
  styleUrls: ['./open-account.component.css']
})
export class OpenAccountComponent implements OnInit {

  constructor(private data: DataServiceService ,  private _router: Router) { }

  ngOnInit() {
  }
  Account = new FormGroup({
    fullname: new FormControl(''),
    email: new FormControl(''),
    homeAddress: new FormControl(''),
    telephone: new FormControl(''),
    accountType: new FormControl(''),
    accountBalance: new FormControl(0.0),
    jointHolder : new FormControl(''),
  });
  openAccount(){
    this.data.openAccount(this.Account.value);
    this._router.navigateByUrl ('/OpenAccount');
  }
}
