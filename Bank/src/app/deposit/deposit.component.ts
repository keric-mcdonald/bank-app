import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {ActivatedRoute} from "@angular/router";
import {DataServiceService} from "../data-service.service";

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {

  constructor(private data: DataServiceService, private _route : ActivatedRoute) { }
  account:any;
  Deposit = new FormGroup({
    deposit : new FormControl(''),
    id : new FormControl(0)
  })

  ngOnInit() {    
    this._route.paramMap.subscribe(params => {
     this.data.getAccount(params.get("id")).subscribe(data => {
      this.account = data;
     })
    });
   
  }

  deposit(){
    this.Deposit.controls['id'].setValue(this.account.accountNumber);
    this.data.deposit(this.Deposit.value).subscribe(data => {
      this.account = data;

    })
  }

}
