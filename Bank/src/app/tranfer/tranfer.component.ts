import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import {ActivatedRoute} from "@angular/router";
import {DataServiceService} from "../data-service.service";

@Component({
  selector: 'app-tranfer',
  templateUrl: './tranfer.component.html',
  styleUrls: ['./tranfer.component.css']
})
export class TranferComponent implements OnInit {
  accounts: any;
  constructor(private data: DataServiceService, private _route : ActivatedRoute) { }
  Transfer  = new FormGroup({
    from : new FormControl(''),
    to : new FormControl(''),
    amount : new FormControl('')

  })
  ngOnInit() {
    this.data.getAccounts().subscribe(data => {
      this.accounts = data;
    })
  }
  transfer(){
    console.log(this.Transfer.value);
    this.data.transfer(this.Transfer.value).subscribe(data => {
      this.accounts = data;
    });
  }
  

}
