import { Component, OnInit } from '@angular/core';
import {DataServiceService} from "../data-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-view-accounts',
  templateUrl: './view-accounts.component.html',
  styleUrls: ['./view-accounts.component.css']
})
export class ViewAccountsComponent implements OnInit {  
  constructor(private data: DataServiceService , private _router: Router) { }
  Accounts : object;

  ngOnInit() {
    this.data.getAccounts().subscribe(data => {
        this.Accounts = data;
    })
    
  }

  deposit(id:number){
    this._router.navigate(['/Deposit', id]);
  }
  withdraw(id:number){
    this._router.navigate(['/Withdraw', id]);
  }


}
