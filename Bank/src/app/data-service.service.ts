import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class DataServiceService {
  apiUrl: string = "http://localhost:8080/Accounts"; 
  params : any ;
  constructor(private http: HttpClient) { }
 
  openAccount(account: JSON){
    this.http.post<JSON>(this.apiUrl ,account ).subscribe();    
  }
  deposit(deposit: any){
    return this.http.put<JSON>(this.apiUrl + "/Deposit",deposit);    
  }
  withdraw(deposit: any){
    return this.http.put<JSON>(this.apiUrl + "/withdraw",deposit);    
  }
  transfer(transfer: any){
    return this.http.put<JSON>(this.apiUrl + "/transfer",transfer);    
  }
  getAccounts(){    
   return this.http.get(this.apiUrl);
  }

  getAccount(id:string){ 
    return this.http.get(this.apiUrl + "/" + id );
   }

}
