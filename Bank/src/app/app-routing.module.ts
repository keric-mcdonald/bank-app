import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";
import {OpenAccountComponent} from "./open-account/open-account.component";
import {RegisterComponent} from "./register/register.component";
import {ViewAccountsComponent} from "./view-accounts/view-accounts.component";
import {DepositComponent} from "./deposit/deposit.component";
import {WithdrawComponent} from "./withdraw/withdraw.component";
import {TranferComponent} from "./tranfer/tranfer.component";


const routes: Routes = [
  {path : "", component: OpenAccountComponent},
  {path : "Login", component: LoginComponent},
  {path : "OpenAccount", component: OpenAccountComponent},
  {path : "Register", component: RegisterComponent},
  {path : "ViewAccounts", component: ViewAccountsComponent},
  {path : "Deposit/:id", component: DepositComponent},
  {path : "Withdraw/:id", component: WithdrawComponent},
  {path : "Transfer", component: TranferComponent},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
