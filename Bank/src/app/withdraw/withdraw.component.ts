import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {ActivatedRoute} from "@angular/router";
import {DataServiceService} from "../data-service.service";

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {

  constructor(private data: DataServiceService, private _route : ActivatedRoute) { }
  account:any;
  Withdraw = new FormGroup({
    deposit : new FormControl(''),
    id : new FormControl(0)
  })

  ngOnInit() {
    this._route.paramMap.subscribe(params => {
      this.data.getAccount(params.get("id")).subscribe(data => {
       this.account = data;
      })
     });
  }

  withdraw(){
    this.Withdraw.controls['id'].setValue(this.account.accountNumber);
    this.data.withdraw(this.Withdraw.value).subscribe(data => {
      this.account = data;

    })
  }

}
